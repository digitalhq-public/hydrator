<?php

namespace Digitalhq\Hydrator;

use Illuminate\Support\ServiceProvider;

/**
 * Class HydratorServiceProvider
 * @package Digitalhq\Hydrator
 */
class HydratorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([MakeMapperCommand::class]);
        }
    }

}