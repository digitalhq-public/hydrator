<?php

namespace Digitalhq\Hydrator;

use Illuminate\Console\GeneratorCommand;

/**
 * Class MakeMapperCommand
 * @package App\Console\Commands
 */
class MakeMapperCommand extends GeneratorCommand
{

    /** @var string */
    protected $signature = 'make:mapper {name}';

    /** @var string */
    protected $description = 'Create new mapper class for hydrator';

    /** @var string */
    protected $type = 'Mapper';

    /**
     * @return string
     */
    protected function getStub() : string
    {
        return
            __DIR__
            . DIRECTORY_SEPARATOR
            . 'stubs'
            . DIRECTORY_SEPARATOR
            . 'mapper.stub'
        ;
    }

    /**
     * @param string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace) : string
    {
        return $rootNamespace . '\Mappers';
    }

}
