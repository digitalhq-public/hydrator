<?php

use Digitalhq\Hydrator\DataMapperInterface;

if (!function_exists('hydrator')) {
    function hydrator($data, DataMapperInterface $mapper = null)
    {
        return new Digitalhq\Hydrator\Hydrator($data, $mapper);
    }
}