<?php

namespace Digitalhq\Hydrator\Exceptions;

/**
 * Class MapperNotSetException
 * @package App\Hydrator\Exceptions
 */
class MapperNotSetException extends \Exception
{

    /** @var string */
    protected $message = 'Mapper class not set';

}
