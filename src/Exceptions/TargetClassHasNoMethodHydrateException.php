<?php

namespace Digitalhq\Hydrator\Exceptions;

/**
 * Class TargetClassHasNoMethodHydrateException
 * @package Digitalhq\Hydrator\Exceptions
 */
class TargetClassHasNoMethodHydrateException extends \Exception
{

    /** @var string */
    protected $message = 'Target class has no hydrate method';

}