<h1>Laravel mapping and hydration package</h1>

<h2>Installation</h2>

~~~bash
composer require digitalhq/hydrator
~~~

After the install package add the service provider class to config/app.php

~~~php
'providers' => [
    Digitalhq\Hydrator\HydratorServiceProvider::class,
],
~~~

<h2>Usage</h2>

<h3>Generate data mapper</h3>

~~~bash
php artisan make:mapper MyCustomDataMapper
~~~

After that generator makes the mapper class, like

~~~php
<?php

namespace App\Mappers;

use Digitalhq\Hydrator\DataMapperInterface;

class MyMapper implements DataMapperInterface
{

    public function transform(array $data) : array
    {
        return [
            // Map properties here
        ];
    }

}
~~~

<h3>Data mapping</h3>

~~~php
// Map data array
$data = ['id' => 1, 'name' => 'Test 1'];

$mapped_collection = hydrator($data)->with(new MyMapper)->hydrate();
// or 
$mapped_collection = hydrator($data, new MyMapper)->hydrate();

// Map array of data array
$data = [
    ['id' => 1, 'name' => 'Test 1'],
    ['id' => 2, 'name' => 'Test 2'],
];
$mapped_collection = hydrator($data)->with(new MyMapper)->hydrate();
~~~

<h3>Data hydrate to models</h3>

~~~php
// Map array of data array
$data = [
    ['id' => 1, 'name' => 'Test 1'],
    ['id' => 2, 'name' => 'Test 2'],
];
$mapped_collection = hydrator($data)->with(new MyMapper)->to(User::class)->hydrate();
~~~